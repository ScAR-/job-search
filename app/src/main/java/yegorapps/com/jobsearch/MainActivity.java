package yegorapps.com.jobsearch;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;

import com.arellomobile.mvp.MvpAppCompatActivity;
import com.arellomobile.mvp.MvpAppCompatFragment;

import yegorapps.com.jobsearch.model.HHVacancy;
import yegorapps.com.jobsearch.model.HHVacancyShort;
import yegorapps.com.jobsearch.services.HHSearchService;
import yegorapps.com.jobsearch.ui.details.DetailsFragment;
import yegorapps.com.jobsearch.ui.search.SearchFragment;

public class MainActivity extends MvpAppCompatActivity implements SearchFragment.VacancyLoadingListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        HHSearchService.initService(this);
        showFragment(new SearchFragment());
    }

    public void showFragment(MvpAppCompatFragment f) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.container, f);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }

    @Override
    public void showVacancyDetails(HHVacancyShort vacancy) {
        DetailsFragment fragment = new DetailsFragment();
        fragment.setVacancyId(vacancy.getId());
        showFragment(fragment);
    }
}
