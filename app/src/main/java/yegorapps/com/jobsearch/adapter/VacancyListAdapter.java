package yegorapps.com.jobsearch.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import yegorapps.com.jobsearch.R;
import yegorapps.com.jobsearch.model.HHVacancyShort;

public class VacancyListAdapter extends RecyclerView.Adapter<VacancyListAdapter.VacancyViewHolder> {
    public interface VacancyListAdapterListener {
        void onItemClick(HHVacancyShort vacancy);
        void loadMoreVacancies();
        void checkForUpdates();
        void stopUpdates();
    }

    private List<HHVacancyShort> vacancies;
    private VacancyListAdapterListener listener;

    public VacancyListAdapter(List<HHVacancyShort> vacancies, VacancyListAdapterListener listener) {
        super();
        this.vacancies = vacancies;
        this.listener = listener;
    }

    public void addVacancies(List<HHVacancyShort> vacancies) {
        this.vacancies.addAll(vacancies);
        notifyDataSetChanged();
    }

    @Override
    public VacancyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new VacancyViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.vacancy_list_item_view, parent, false));
    }

    @Override
    public void onBindViewHolder(VacancyViewHolder holder, int position) {
        if (position == 0) {
            listener.checkForUpdates();
        } else {
            listener.stopUpdates();
        }

        if (position == vacancies.size() - 1) {
            listener.loadMoreVacancies();
        }

        final HHVacancyShort vacancy = getItem(position);

        holder.vacancyName.setText(vacancy.getVacancyName());
        holder.employerName.setText(vacancy.getEmployerName());
        holder.snippetText.setText(vacancy.getTextShort());
        holder.publishedAt.setText(vacancy.getPublishedAt());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onItemClick(vacancy);
            }
        });
    }

    @Override
    public int getItemCount() {
        return vacancies.size();
    }

    public HHVacancyShort getItem(int position) {
        return vacancies.get(position);
    }

    class VacancyViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.vacancy_name)
        TextView vacancyName;
        @BindView(R.id.vacancy_employer_name)
        TextView employerName;
        @BindView(R.id.vacancy_snippet)
        TextView snippetText;
        @BindView(R.id.vacancy_published_at)
        TextView publishedAt;

        public VacancyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
