package yegorapps.com.jobsearch.interactor;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.annotations.NonNull;
import io.reactivex.functions.Function;
import yegorapps.com.jobsearch.model.HHVacancy;
import yegorapps.com.jobsearch.model.HHVacancyShort;
import yegorapps.com.jobsearch.services.entity.HHVacanciesSearchResponseEntity;
import yegorapps.com.jobsearch.services.entity.HHVacancyLongEntity;
import yegorapps.com.jobsearch.services.entity.HHVacancyShortEntity;
import yegorapps.com.jobsearch.repository.IVacancyRepository;
import yegorapps.com.jobsearch.repository.VacancyRepository;

public class VacancyInteractor {
    private IVacancyRepository repository = new VacancyRepository();

    public Observable<List<HHVacancyShort>> performSearch(String queryText, Integer page) {
        return repository.performSearch(queryText, page).map(new Function<HHVacanciesSearchResponseEntity, List<HHVacancyShort>>() {
            @Override
            public List<HHVacancyShort> apply(@NonNull HHVacanciesSearchResponseEntity hhVacanciesSearchResponse) throws Exception {
                List<HHVacancyShort> vacancies = new ArrayList<>();
                for (HHVacancyShortEntity vacancy : hhVacanciesSearchResponse.vacancies) {
                    vacancies.add(new HHVacancyShort(vacancy));
                }
                return vacancies;
            }
        });
    }

    public Observable<HHVacancy> getVacancyById(String id) {
        return repository.getVacancyById(id).map(new Function<HHVacancyLongEntity, HHVacancy>() {
            @Override
            public HHVacancy apply(@NonNull HHVacancyLongEntity vacancyLongEntity) throws Exception {
                return new HHVacancy(vacancyLongEntity);
            }
        });
    }
}
