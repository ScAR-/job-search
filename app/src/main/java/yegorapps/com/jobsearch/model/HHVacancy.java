package yegorapps.com.jobsearch.model;

import yegorapps.com.jobsearch.services.entity.HHVacancyLongEntity;

public class HHVacancy {
    private String vacancyName;
    private String employerName;
    private String description;
    private String city;
    private String occupation;
    private Integer salaryFrom;
    private Integer salaryTo;
    private String currency;
    private String logo;

    public HHVacancy(HHVacancyLongEntity vacancy) {
        if (vacancy == null) return;

        vacancyName = vacancy.name;
        description = vacancy.description;

        if (vacancy.employer != null) {
            employerName = vacancy.employer.name;

            if (vacancy.employer.logoUrls != null)
                logo = vacancy.employer.logoUrls.url240;
        }

        if (vacancy.area != null)
            city = vacancy.area.city;

        if (vacancy.employment != null)
            occupation = vacancy.employment.name;

        if (vacancy.salary != null) {
            salaryFrom = vacancy.salary.from;
            salaryTo = vacancy.salary.to;
            currency = vacancy.salary.currency;
        }
    }

    public String getVacancyName() {
        return vacancyName;
    }

    public String getEmployerName() {
        return employerName;
    }

    public String getDescription() {
        return description;
    }

    public String getCity() {
        return city;
    }

    public String getOccupation() {
        return occupation;
    }

    public Integer getSalaryFrom() {
        return salaryFrom;
    }

    public Integer getSalaryTo() {
        return salaryTo;
    }

    public String getCurrency() {
        return currency;
    }

    public String getLogo() {
        return logo;
    }
}
