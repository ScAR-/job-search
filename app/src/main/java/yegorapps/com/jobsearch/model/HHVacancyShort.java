package yegorapps.com.jobsearch.model;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import yegorapps.com.jobsearch.services.entity.HHVacancyShortEntity;

public class HHVacancyShort {
    private String id;
    private String vacancyName;
    private String textShort;
    private String employerName;
    private String publishedAt;

    public HHVacancyShort(HHVacancyShortEntity vacancy) {
        if (vacancy == null) return;

        id = vacancy.id;
        vacancyName = vacancy.name;

        if (vacancy.employer != null) {
            employerName = vacancy.employer.name;
        }

        if (vacancy.snippet != null) {
            textShort = vacancy.snippet.responsibility;
        }

        //2017-05-21T16:06:51+0300
        Calendar c = Calendar.getInstance();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ", Locale.US);
        SimpleDateFormat neededFormat = new SimpleDateFormat("dd.MM.yyyy", Locale.US);
        try {
            c.setTime(simpleDateFormat.parse(vacancy.publishedAt));
            publishedAt = neededFormat.format(c.getTimeInMillis());

        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    public String getVacancyName() {
        return vacancyName;
    }

    public String getTextShort() {
        return textShort;
    }

    public String getEmployerName() {
        return employerName;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPublishedAt() {
        return publishedAt;
    }
}
