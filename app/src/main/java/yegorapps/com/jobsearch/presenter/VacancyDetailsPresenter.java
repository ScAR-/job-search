package yegorapps.com.jobsearch.presenter;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;

import java.util.List;

import io.reactivex.annotations.NonNull;
import io.reactivex.observers.DisposableObserver;
import yegorapps.com.jobsearch.interactor.VacancyInteractor;
import yegorapps.com.jobsearch.model.HHVacancy;
import yegorapps.com.jobsearch.model.HHVacancyShort;
import yegorapps.com.jobsearch.view.DetailsView;
import yegorapps.com.jobsearch.view.SearchView;

@InjectViewState
public class VacancyDetailsPresenter extends MvpPresenter<DetailsView> {
    private VacancyInteractor interactor = new VacancyInteractor();

    public void getVacancyById(String id) {
        if (getViewState() != null) {
            getViewState().showProgressLoading();
            interactor.getVacancyById(id).subscribe(new DisposableObserver<HHVacancy>() {
                @Override
                public void onNext(@NonNull HHVacancy vacancy) {
                    getViewState().showVacancyDetails(vacancy);
                    getViewState().hideProgressLoading();
                }

                @Override
                public void onError(@NonNull Throwable e) {
                    getViewState().hideProgressLoading();
                    e.printStackTrace();
                }

                @Override
                public void onComplete() {

                }
            });
        }
    }
}
