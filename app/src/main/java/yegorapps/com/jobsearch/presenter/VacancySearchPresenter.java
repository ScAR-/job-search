package yegorapps.com.jobsearch.presenter;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;

import java.util.List;

import io.reactivex.annotations.NonNull;
import io.reactivex.observers.DisposableObserver;
import yegorapps.com.jobsearch.interactor.VacancyInteractor;
import yegorapps.com.jobsearch.model.HHVacancyShort;
import yegorapps.com.jobsearch.view.SearchView;

@InjectViewState
public class VacancySearchPresenter extends MvpPresenter<SearchView> {
    private VacancyInteractor interactor = new VacancyInteractor();

    public void performSearch(String searchQuery, Integer page) {
        if (getViewState() != null) {
            getViewState().showProgressLoading();
            interactor.performSearch(searchQuery, page).subscribe(new DisposableObserver<List<HHVacancyShort>>() {
                @Override
                public void onNext(@NonNull List<HHVacancyShort> hhVacancyShorts) {
                    getViewState().showSearchResults(hhVacancyShorts);
                    getViewState().hideProgressLoading();
                }

                @Override
                public void onError(@NonNull Throwable e) {
                    getViewState().hideProgressLoading();
                    e.printStackTrace();
                }

                @Override
                public void onComplete() {

                }
            });
        }
    }
}
