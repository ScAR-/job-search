package yegorapps.com.jobsearch.repository;

import io.reactivex.Observable;
import yegorapps.com.jobsearch.services.entity.HHVacanciesSearchResponseEntity;
import yegorapps.com.jobsearch.services.entity.HHVacancyLongEntity;

public interface IVacancyRepository {
    Observable<HHVacanciesSearchResponseEntity> performSearch(String queryText, Integer page);
    Observable<HHVacancyLongEntity> getVacancyById(String id);
}
