package yegorapps.com.jobsearch.repository;

import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.schedulers.Schedulers;
import yegorapps.com.jobsearch.services.entity.HHVacanciesSearchResponseEntity;
import yegorapps.com.jobsearch.services.HHSearchService;
import yegorapps.com.jobsearch.services.entity.HHVacancyLongEntity;

public class VacancyRepository implements IVacancyRepository {
    @Override
    public Observable<HHVacanciesSearchResponseEntity> performSearch(final String queryText, final Integer page) {
        return Observable.create(new ObservableOnSubscribe<HHVacanciesSearchResponseEntity>() {
            @Override
            public void subscribe(@NonNull ObservableEmitter<HHVacanciesSearchResponseEntity> e) {
                try {
                    HHVacanciesSearchResponseEntity response = HHSearchService.getVacancies(queryText, page);
                    e.onNext(response);
                    e.onComplete();
                } catch (Exception e1) {
                    e.onError(e1);
                }
            }
        }).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread());
    }

    @Override
    public Observable<HHVacancyLongEntity> getVacancyById(final String id) {
        return Observable.create(new ObservableOnSubscribe<HHVacancyLongEntity>() {
            @Override
            public void subscribe(@NonNull ObservableEmitter<HHVacancyLongEntity> e) {
                try {
                    HHVacancyLongEntity entity = HHSearchService.getVacancy(id);
                    e.onNext(entity);
                    e.onComplete();
                } catch (Exception e1) {
                    e.onError(e1);
                }
            }
        }).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread());
    }
}
