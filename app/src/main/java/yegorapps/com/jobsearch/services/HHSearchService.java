package yegorapps.com.jobsearch.services;

import android.content.Context;
import android.util.Log;

import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import yegorapps.com.jobsearch.R;
import yegorapps.com.jobsearch.services.entity.HHVacanciesSearchResponseEntity;
import yegorapps.com.jobsearch.services.entity.HHVacancyLongEntity;

public class HHSearchService {
    private static IHHSearchService hhSearchService;

    public static void initService(Context context) {
        if (hhSearchService == null) {
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(context.getString(R.string.base_api_url))
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
            hhSearchService = retrofit.create(IHHSearchService.class);
        }
    }

    public static HHVacanciesSearchResponseEntity getVacancies(String searchQuery, Integer page) throws Exception {
        return new Caller<HHVacanciesSearchResponseEntity>().callSync(hhSearchService.getVacancies(searchQuery, String.valueOf(page)));
    }

    public static HHVacancyLongEntity getVacancy(String id) throws Exception {
        return new Caller<HHVacancyLongEntity>().callSync(hhSearchService.getVacancyById(id));
    }

    private static class Caller<T> {
        T callSync(Call<T> call) throws Exception {
            try {
                Response<T> response = call.execute();
                if (response.isSuccessful()) {
                    return response.body();
                }
                else {
                    throw new Exception(response.raw().message());
                }
            } catch (Exception e){
                Log.e(e.getMessage(), e.getLocalizedMessage());
                throw new Exception(e);
            }
        }
    }
}
