package yegorapps.com.jobsearch.services;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;
import yegorapps.com.jobsearch.services.entity.HHVacanciesSearchResponseEntity;
import yegorapps.com.jobsearch.services.entity.HHVacancyLongEntity;

public interface IHHSearchService {
    @GET("/vacancies")
    Call<HHVacanciesSearchResponseEntity> getVacancies(@Query("text") String searchQuery, @Query("page") String page);

    @GET("/vacancies/{id}")
    Call<HHVacancyLongEntity> getVacancyById(@Path("id") String id);

}