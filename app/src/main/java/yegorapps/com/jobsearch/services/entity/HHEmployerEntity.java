package yegorapps.com.jobsearch.services.entity;

import com.google.gson.annotations.SerializedName;

public class HHEmployerEntity {
    @SerializedName("name")
    public String name;

    @SerializedName("logo_urls")
    public LogoUrls logoUrls;

    public class LogoUrls {
        @SerializedName("240")
        public String url240;
    }
}
