package yegorapps.com.jobsearch.services.entity;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class HHVacanciesSearchResponseEntity {
    @SerializedName("items")
    public List<HHVacancyShortEntity> vacancies;
}
