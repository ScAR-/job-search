package yegorapps.com.jobsearch.services.entity;

import com.google.gson.annotations.SerializedName;

public class HHVacancyLongEntity {
    @SerializedName("name")
    public String name;

    @SerializedName("employer")
    public HHEmployerEntity employer;

    @SerializedName("description")
    public String description;

    @SerializedName("salary")
    public HHVacancySalary salary;

    @SerializedName("area")
    public HHVacancyArea area;

    @SerializedName("employment")
    public HHVacancyEmployment employment;

    public class HHVacancyArea {
        @SerializedName("name")
        public String city;
    }

    public class HHVacancySalary {
        @SerializedName("from")
        public Integer from;

        @SerializedName("to")
        public Integer to;

        @SerializedName("currency")
        public String currency;
    }

    public class HHVacancyEmployment {
        @SerializedName("name")
        public String name;
    }
}
