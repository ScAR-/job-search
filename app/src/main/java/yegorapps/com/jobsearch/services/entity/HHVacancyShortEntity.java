package yegorapps.com.jobsearch.services.entity;

import com.google.gson.annotations.SerializedName;

public class HHVacancyShortEntity {
    @SerializedName("id")
    public String id;

    @SerializedName("name")
    public String name;

    @SerializedName("snippet")
    public HHVacancySnippet snippet;

    @SerializedName("published_at")
    public String publishedAt;

    @SerializedName("employer")
    public HHEmployerEntity employer;

    public class HHVacancySnippet {
        @SerializedName("responsibility")
        public String responsibility;
    }
}
