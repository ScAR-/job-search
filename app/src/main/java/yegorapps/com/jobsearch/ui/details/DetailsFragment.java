package yegorapps.com.jobsearch.ui.details;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.arellomobile.mvp.MvpAppCompatFragment;
import com.arellomobile.mvp.presenter.InjectPresenter;
import com.squareup.picasso.Picasso;

import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import yegorapps.com.jobsearch.R;
import yegorapps.com.jobsearch.model.HHVacancy;
import yegorapps.com.jobsearch.presenter.VacancyDetailsPresenter;
import yegorapps.com.jobsearch.view.DetailsView;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;

public class DetailsFragment extends MvpAppCompatFragment implements DetailsView {
    @BindView(R.id.vacancy_employer_logo)
    ImageView logo;

    @BindView(R.id.vacancy_employer_city)
    TextView city;

    @BindView(R.id.vacancy_employer_name)
    TextView employerName;

    @BindView(R.id.vacancy_name)
    TextView vacancyName;

    @BindView(R.id.vacancy_salary_from)
    TextView salaryFrom;

    @BindView(R.id.vacancy_salary_to)
    TextView salaryTo;

    @BindView(R.id.vacancy_occupation)
    TextView occupation;

    @BindView(R.id.progressBar)
    ProgressBar progressBar;

    @InjectPresenter
    VacancyDetailsPresenter presenter;

    private String vacancyId;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_details, container, false);
        ButterKnife.bind(this, v);
        presenter.getVacancyById(vacancyId);
        return v;
    }

    public void setVacancyId(String vacancyId) {
        this.vacancyId = vacancyId;
    }

    @Override
    public void showProgressLoading() {
        progressBar.setVisibility(VISIBLE);
    }

    @Override
    public void hideProgressLoading() {
        progressBar.setVisibility(GONE);
    }

    @Override
    public void showVacancyDetails(HHVacancy vacancy) {
        Picasso.with(getActivity()).load(vacancy.getLogo()).into(logo);
        city.setText(vacancy.getCity());
        employerName.setText(vacancy.getEmployerName());
        vacancyName.setText(vacancy.getVacancyName());
        salaryFrom.setText(String.format(Locale.US, "От: %d %s", vacancy.getSalaryFrom(), vacancy.getCurrency()));
        salaryTo.setText(String.format(Locale.US, "До: %d %s", vacancy.getSalaryTo(), vacancy.getCurrency()));
        occupation.setText(vacancy.getOccupation());

        if (vacancy.getSalaryFrom() == null)
            salaryFrom.setVisibility(GONE);
        if (vacancy.getSalaryTo() == null)
            salaryTo.setVisibility(GONE);
    }
}
