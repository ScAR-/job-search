package yegorapps.com.jobsearch.ui.search;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ProgressBar;

import com.arellomobile.mvp.MvpAppCompatFragment;
import com.arellomobile.mvp.presenter.InjectPresenter;

import java.util.List;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import io.reactivex.Observable;
import io.reactivex.annotations.NonNull;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;
import yegorapps.com.jobsearch.R;
import yegorapps.com.jobsearch.adapter.VacancyListAdapter;
import yegorapps.com.jobsearch.model.HHVacancyShort;
import yegorapps.com.jobsearch.presenter.VacancySearchPresenter;
import yegorapps.com.jobsearch.view.SearchView;

public class SearchFragment extends MvpAppCompatFragment implements SearchView, VacancyListAdapter.VacancyListAdapterListener {
    public interface VacancyLoadingListener {
        void showVacancyDetails(HHVacancyShort vacancy);
    }

    @BindView(R.id.query_edittext)
    EditText queryEditText;

    @BindView(R.id.suggestions_list)
    RecyclerView vacanciesList;

    @BindView(R.id.progressBar)
    ProgressBar progressBar;

    @InjectPresenter
    VacancySearchPresenter presenter;

    private VacancyLoadingListener listener;
    private VacancyListAdapter adapter;
    private DisposableObserver<Long> observer;
    private Integer page = 0;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_search, container, false);
        ButterKnife.bind(this, v);
        listener = (VacancyLoadingListener) getActivity();
        initObserver();
        return v;
    }

    @OnClick(R.id.search_button)
    void performSearch() {
        page = 0;
        String searchQuery = "";
        if (queryEditText.getText() != null)
            searchQuery = queryEditText.getText().toString();
        presenter.performSearch(searchQuery, page);
        hideKeyboard(queryEditText);
    }

    private void initObserver() {
        observer = new DisposableObserver<Long>() {
            @Override
            public void onNext(@NonNull Long aLong) {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        String searchQuery = "";
                        if (queryEditText.getText() != null)
                            searchQuery = queryEditText.getText().toString();
                        presenter.performSearch(searchQuery, 0);
                    }
                });
            }

            @Override
            public void onError(@NonNull Throwable e) {

            }

            @Override
            public void onComplete() {

            }
        };
    }

    @Override
    public void showSearchResults(final List<HHVacancyShort> vacancies) {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (adapter == null) {
                    adapter = new VacancyListAdapter(vacancies, SearchFragment.this);
                    vacanciesList.setAdapter(adapter);
                    vacanciesList.setLayoutManager(new LinearLayoutManager(getActivity()));
                } else {
                    adapter.addVacancies(vacancies);
                }
            }
        });
    }

    public void hideKeyboard(View v) {
        InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
    }

    @Override
    public void onItemClick(HHVacancyShort vacancy) {
        listener.showVacancyDetails(vacancy);
    }

    @Override
    public void loadMoreVacancies() {
        String searchQuery = "";
        if (queryEditText.getText() != null)
            searchQuery = queryEditText.getText().toString();
        presenter.performSearch(searchQuery, page++);
    }

    @Override
    public void checkForUpdates() {
        if (!observer.isDisposed()) return;
        Observable.interval(5, TimeUnit.SECONDS, Schedulers.io()).subscribe(observer);
    }

    @Override
    public void stopUpdates() {
        if (observer != null) {
            observer.dispose();
        }
    }

    @Override
    public void showProgressLoading() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgressLoading() {
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void onPause() {
        super.onPause();
        adapter = null;
    }
}
