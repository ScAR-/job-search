package yegorapps.com.jobsearch.view;

import com.arellomobile.mvp.MvpView;

public interface BaseView extends MvpView {
    void showProgressLoading();
    void hideProgressLoading();
}
