package yegorapps.com.jobsearch.view;

import yegorapps.com.jobsearch.model.HHVacancy;

public interface DetailsView extends BaseView {
    void showVacancyDetails(HHVacancy vacancy);
}
