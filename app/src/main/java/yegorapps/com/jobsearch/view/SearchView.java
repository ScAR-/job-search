package yegorapps.com.jobsearch.view;

import java.util.List;

import yegorapps.com.jobsearch.model.HHVacancyShort;

public interface SearchView extends BaseView {
    void showSearchResults(List<HHVacancyShort> vacancies);
}
